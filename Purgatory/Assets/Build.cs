﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Build : MonoBehaviour
{
    public bool done = false;
    public string url;

    public List<string> gameObjectString;

    void Start()
    {
        StartCoroutine(GetAssetBundle());
        Debug.Log("pushed");
    }

    IEnumerator GetAssetBundle()
    {
        WWW www = new WWW(url);
        yield return www;

        Debug.Log("entered");

        AssetBundle bundle = www.assetBundle;
        if (www.error == null)
        {
            GameObject arena = (GameObject)bundle.LoadAsset(gameObjectString[0]);
            Instantiate(arena);
        }
        else
            Debug.Log(www.error);

        done = true;
        Debug.Log("finished");
    }
}