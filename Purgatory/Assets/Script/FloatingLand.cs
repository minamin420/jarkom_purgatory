﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingLand : MonoBehaviour {

	private PlatformEffector2D effector;
	private float waitTime;

	// Use this for initialization
	void Start () {
		effector = GetComponent<PlatformEffector2D> ();
		waitTime = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.DownArrow)) {
			if (waitTime <= 0) {
				effector.rotationalOffset = 180f;
				waitTime = 0.5f;
			} else {
				waitTime -= Time.deltaTime;
			}
		}

		if(Input.GetKey(KeyCode.Space)){
			effector.rotationalOffset = 0;
		}
	}
}
