﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

	public float destroyTime;
	public PlayerHP player;
	private float waitTime;

	void Start () {
		waitTime = destroyTime;	
		player = GameObject.Find("Playerr").GetComponent<PlayerHP> ();
	}
	
	void Update () {
		if (waitTime <= 0) {
			Destroy (gameObject);
			waitTime = destroyTime;
		} else {
			waitTime -= Time.deltaTime;
		}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Player")){
			Destroy (gameObject);
			player.health += 20;
			if (player.health > 100) {
				player.health = 100;
			}
		}
	}
}
