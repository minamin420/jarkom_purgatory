﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Networking;
using System;

namespace Project.Player
{
    public class PlayerManager : MonoBehaviour
    {
        [Header("Data")]
        [SerializeField]
        private float speed = 5;
        public float jumpForce = 5f;
        public int jumpTime;
        public LayerMask whatIsGround;
        public Transform feetPos;
        public float checkRadius;
        private float moveInput;
        private bool isGrounded;
        private int jump;
        private Rigidbody2D rb;
        private Animator anim;
        [SerializeField]
        private float rotation = 180;

        [Header("Class References")]
        [SerializeField]
        private NetworkIdentity networkIdentity;

        private float lastRotation;

        public void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            jump = jumpTime;
        }

        public void Update()
        {
            if (networkIdentity.IsControlling())
            {
                checkMovement();
            }
        }

        private void checkMovement()
        {
            moveInput = Input.GetAxis("Horizontal");
            rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
            isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                transform.rotation = Quaternion.Euler(0, rotation, 0);
            }

            if (Input.GetKeyDown(KeyCode.Space) && jump > 1)
            {
                rb.velocity = Vector2.up * jumpForce;
                jump -= 1;
            }

            if (isGrounded)
            {
                jump = jumpTime;
            }

            CharacterAnimation();
        }

        public float GetLastRotation()
        {
            return lastRotation;
        }
        
        void CharacterAnimation()
        {
            if (moveInput > 0 || moveInput < 0)
            {
                anim.SetBool("isRedKnightRun", true);
            }
            else
            {
                anim.SetBool("isRedKnightRun", false);
            }
        }
    }
}
