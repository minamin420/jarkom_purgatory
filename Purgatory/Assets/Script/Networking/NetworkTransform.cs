﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Utility.Attributes;
using Project.Utility;
using Project.Player;
using Project.Player.Attack;

namespace Project.Networking
{
    [RequireComponent(typeof(NetworkIdentity))]
    public class NetworkTransform : MonoBehaviour
    {
        [SerializeField]
        [GrayOut]
        private Vector3 oldPos;

        private NetworkIdentity networkIdentity;
        private Player player;
        private PlayerManager manager;
        private PlayerAttack attack;

        private float stillCounter = 0;

        public void Start()
        {
            networkIdentity = GetComponent<NetworkIdentity>();
            oldPos = transform.position;
            player = new Player();
            player.position = new Position();
            player.position.x = 0;
            player.position.y = 0;

            if (!networkIdentity.IsControlling())
            {
                enabled = false;
            }
        }

        void Update()
        {
            if (networkIdentity.IsControlling())
            {
                if(oldPos != transform.position)
                {
                    oldPos = transform.position;
                    stillCounter = 0;
                    sendData();
                } else
                {
                    stillCounter += Time.deltaTime;

                    if(stillCounter >= 1)
                    {
                        stillCounter = 0;
                        sendData();
                    }
                }
            }
        }

        private void sendData()
        {
            player.position.x = transform.position.x.TwoDecimals();
            player.position.y = transform.position.y.TwoDecimals();

            networkIdentity.GetSocket().Emit("updatePosition", new JSONObject(JsonUtility.ToJson(player)));
        }
    }
}
