﻿using Project.Player;
using Project.Utility;
using Project.Utility.Attributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Networking
{
    [RequireComponent(typeof(NetworkIdentity))]
    public class NetworkRotation : MonoBehaviour
    {
        [Header("Referenced Values")]
        [SerializeField]
        [GrayOut]
        private float oldPlayerRotation;

        [Header("Class References")]
        [SerializeField]
        private NetworkIdentity networkIdentity;
        private PlayerManager player;
        private PlayerRotation playerRot;

        private float stillCounter = 0;
        // Start is called before the first frame update
        public void Start()
        {
            networkIdentity = GetComponent<NetworkIdentity>();

            playerRot = new PlayerRotation();
            playerRot.playerRotation = 0 ;

            if (!networkIdentity.IsControlling())
            {
                enabled = false;
            }
        }

        // Update is called once per frame
        public void Update()
        {
            if (networkIdentity.IsControlling())
            {
                if (oldPlayerRotation != transform.localEulerAngles.y)
                {
                    oldPlayerRotation = transform.localEulerAngles.y;
                    stillCounter = 0;
                    sendData();
                }
                else
                {
                    stillCounter += Time.deltaTime;
                    if (stillCounter >= 1)
                    {
                        stillCounter = 0;
                        sendData();
                    }
                }
            }
        }
        private void sendData()
        {
            playerRot.playerRotation = transform.localEulerAngles.y.TwoDecimals();

            networkIdentity.GetSocket().Emit("updateRotation", new JSONObject(JsonUtility.ToJson(playerRot)));
        }
    }
}
