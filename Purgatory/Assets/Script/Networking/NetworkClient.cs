﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;
using Project.Utility;
using Project.Scriptable;

namespace Project.Networking
{
    public class NetworkClient : SocketIOComponent
    {
        [Header("Network Client")]
        [SerializeField]
        private Transform networkContainer;
        [SerializeField]
        private GameObject playerPrefab;
        [SerializeField]
        private ServerObjects serverSpawnables; 
        public static string ClientID { get; private set; }

        private Dictionary<string, NetworkIdentity> serverObjects;

        public override void Start()
        {
            base.Start();
            initialize();
            setupEvents();
        }

        private void initialize()
        {
            serverObjects = new Dictionary<string, NetworkIdentity>();
        }

        public override void Update()
        {
            base.Update();
        }
        private void setupEvents()
        {
            On("open", (E) =>
            {
                Debug.Log("Connection Made");
            });

            On("register", (E) =>
            {
                ClientID = E.data["id"].ToString().RemoveQuotes();
                Debug.LogFormat("Client id = " , ClientID);
            });

            On("spawn", (E) =>
            {
                string id = E.data["id"].ToString().RemoveQuotes();

                GameObject gg = Instantiate(playerPrefab, networkContainer);
                gg.name = string.Format("Player {{0}}", id);
                NetworkIdentity nid = gg.GetComponent<NetworkIdentity>();
                nid.SetControllerID(id);
                nid.SetSocketReference(this);
                serverObjects.Add(id, nid);
            });

            On("playerDie", (E) =>
            {
                string id = E.data["id"].ToString().RemoveQuotes();

                
                GameObject gg = serverObjects[id].gameObject;
                NetworkIdentity nid = gg.GetComponent<NetworkIdentity>();
                nid.SetControllerID(id);
                Destroy(gg);
                serverObjects.Remove(id);
            });

            On("disconnected", (E) =>
            {
                string id = E.data["id"].ToString().RemoveQuotes();

                GameObject gg = serverObjects[id].gameObject;
                Destroy(gg);
                serverObjects.Remove(id);
            });

            On("updatePosition", (E) =>
            {
                string id = E.data["id"].ToString().RemoveQuotes();
                float x = E.data["position"]["x"].f;
                float y = E.data["position"]["y"].f;

                NetworkIdentity nid = serverObjects[id];
                nid.transform.position = new Vector3(x, y, 0);
            });

            On("serverSpawn", (E) =>
            {
                string name = E.data["name"].str;
                string id = E.data["id"].ToString().RemoveQuotes();
                float x = E.data["position"]["x"].f;
                float y = E.data["position"]["y"].f;
                Debug.LogFormat("Server Spawns ", name);

                if (!serverObjects.ContainsKey(id))
                {
                    ServerObjectData sod = serverSpawnables.GetObjectByName(name);
                    var spawnedObject = Instantiate(sod.Prefab, networkContainer);
                    spawnedObject.transform.position = new Vector3(x, y, 0);
                    var nid = spawnedObject.GetComponent<NetworkIdentity>();
                    nid.SetControllerID(id);
                    nid.SetSocketReference(this);

                    serverObjects.Add(id, nid);
                }
            });

            On("updateRotation", (E) =>
             {
                 string id = E.data["id"].ToString().RemoveQuotes();
                 float playerRotation = E.data["playerRotation"].f;
                 NetworkIdentity nid = serverObjects[id];
                 nid.transform.localEulerAngles = new Vector3(0, playerRotation, 0);
             });

            On("serverUnspawn", (E) =>
            {
                string id = E.data["id"].ToString().RemoveQuotes();
                NetworkIdentity nid = serverObjects[id];
                serverObjects.Remove(id);
                DestroyImmediate(nid.gameObject);
            });
        }
    }

    [Serializable]
    public class Player
    {
        public string id;
        public Position position;
    }

    [Serializable]
    public class Position
    {
        public float x;
        public float y;
    }

    [Serializable]
    public class ItemData
    {
        public string id;
        public Position position;
    }

    [Serializable]
    public class PlayerRotation
    {
        public float playerRotation;
    }

    [Serializable]
    public class IDData
    {
        public string id;
    }
}
