﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Player
{
    public class PlayerController : MonoBehaviour
    {
        public float speed = 5f;
        public float jumpForce = 5f;
        public int jumpTime;
        public LayerMask whatIsGround;
        public Transform feetPos;
        public float checkRadius;

        private Rigidbody2D rb;
        private float moveInput;
        private bool isGrounded;
        private int jump;

        private Animator anim;

        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            jump = jumpTime;
        }

        void FixedUpdate()
        {
            moveInput = Input.GetAxis("Horizontal");
            rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
        }

        // Update is called once per frame
        void Update()
        {
            isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

            if (moveInput > 0)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
            }
            else if (moveInput < 0)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
            }

            if (Input.GetKeyDown(KeyCode.Space) && jump > 1)
            {
                rb.velocity = Vector2.up * jumpForce;
                jump -= 1;
            }

            if (isGrounded)
            {
                jump = jumpTime;
            }

            CharacterAnimation();
        }

        void CharacterAnimation()
        {
            if (moveInput > 0 || moveInput < 0)
            {
                anim.SetBool("isRedKnightRun", true);
            }
            else
            {
                anim.SetBool("isRedKnightRun", false);
            }
        }
    }
}