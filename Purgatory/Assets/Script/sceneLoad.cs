﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class sceneLoad : MonoBehaviour
{
    private Button This;
    public int sceneID;

    // Start is called before the first frame update
    void Start()
    {
        This = GetComponent<Button>();
        This.onClick.AddListener(move);
    }

    void move()
    {
        SceneManager.LoadScene(sceneID);
    }
}
