﻿using Project.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Player.Attack
{
    public class PlayerAttack : MonoBehaviour
    {
        public Transform attackPos;
        public float attackRangeX;
        public float attackRangeY;
        public int damage;
        private float startWaitBtwAttack = 0.2f;
        private float waitBtwAttack;
        private bool attack;

        [Header("Class References")]
        [SerializeField]
        private NetworkIdentity networkIdentity;

        private Collider2D enemyCollider;

        void Start()
        {
            waitBtwAttack = startWaitBtwAttack;
        }

        void Update()
        {
            if (networkIdentity.IsControlling())
            {
                isAttacking();
            }
        }

        private void isAttacking()
        {
            if (attack)
            {
                if (waitBtwAttack <= 0)
                {
                    enemyCollider = Physics2D.OverlapBox(attackPos.position, new Vector2(attackRangeX, attackRangeY), 0);
                    if (enemyCollider != null)
                    {
                        networkIdentity.GetSocket().Emit("destroy", new JSONObject(JsonUtility.ToJson(new IDData()
                        {
                            id = networkIdentity.GetID()
                        })));
                        enemyCollider.GetComponent<PlayerHP>().takeDamage(10);
                    }
                    waitBtwAttack = startWaitBtwAttack;
                    attack = false;
                }
                else
                {
                    waitBtwAttack -= Time.deltaTime;
                }
            }

            //Attack code here
            if (Input.GetKeyDown(KeyCode.Z))
            {
                GetComponent<Animator>().SetTrigger("Attack");
                attack = true;
            }
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(attackPos.position, new Vector3(attackRangeX, attackRangeY, 1));
        }
    }
}