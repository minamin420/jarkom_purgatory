﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	public PlayerHP player;

	private Image healthBar;
	private float maxHealth = 100f;
	private float health;

	// Use this for initialization
	void Start () {
		healthBar = GetComponent<Image> ();
		health = player.health;
	}
	
	// Update is called once per frame
	void Update () {
		if (health != player.health) {
			if (health > player.health) {
				health -= 1;
				healthBar.fillAmount -= 0.01f;
			} else if (health <= player.health) {
				health += 1;
				healthBar.fillAmount += 0.01f;
			}
		}
	}
}
