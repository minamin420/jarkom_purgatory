﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Networking;
using Project.Player;
using UnityEngine.SceneManagement;

public class PlayerHP : MonoBehaviour
{
    public float health = 100;
    public int sceneID;

    [SerializeField]
    private NetworkIdentity networkIdentity;

    private PlayerManager player;

    public void Start()
    {
        networkIdentity = GetComponent<NetworkIdentity>();

        if (!networkIdentity.IsControlling())
        {
            enabled = false;
        }
    }

    void Update()
    {
        if(health < 1)
        {
            Destroy(this.gameObject);
            sendData();
            SceneManager.LoadScene(sceneID);
        }
    }
		
    public void takeDamage(int damage)
    {
        health -= damage;
    }

   private void sendData()
    {
        networkIdentity.GetSocket().Emit("playerDie", new JSONObject(JsonUtility.ToJson(player)));
    }

    private void OnTriggerEnter2D(Collider2D fall)
    {
        if (fall.CompareTag("Border"))
        {
            Destroy(this.gameObject);
            sendData();
            SceneManager.LoadScene(sceneID);
        }
    }
}
