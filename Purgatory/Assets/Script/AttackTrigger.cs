﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTrigger : MonoBehaviour
{

    public int dmg = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D hit)
    {
        if(hit.isTrigger != true && hit.CompareTag("Player"))
        {
            hit.SendMessageUpwards("Damage", dmg);
        }   

  
    }
}
