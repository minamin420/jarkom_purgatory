﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingLand : MonoBehaviour {

	public float speed;
	public Transform[] moveSpot;
	public float startWaitTime;

	private float waitTime;
	private int nextSpot;

	void Start(){
		waitTime = startWaitTime;
		transform.position = moveSpot [0].position;
	}

	// Update is called once per frame
	void Update () {
		transform.position = Vector2.MoveTowards (transform.position, moveSpot [nextSpot].position, speed * Time.deltaTime);

		if (Vector2.Distance (transform.position, moveSpot [nextSpot].position) < 0.2f) {
			if (waitTime <= 0) {
				if (nextSpot == 1) {
					nextSpot = 0;
				} else {
					nextSpot = 1;
				}
				waitTime = startWaitTime;
			} else {
				waitTime -= Time.deltaTime;
			}
		}
	}
}
