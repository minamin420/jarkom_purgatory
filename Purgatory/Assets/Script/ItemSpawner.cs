﻿using Project.Networking;
using Project.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour {

	public Transform[] spawnPos;
	public GameObject item;
	public float spawnRate;

    [SerializeField]
    private NetworkIdentity networkIdentity;

    private ItemData itemData;

	private int whatToSpawn;
	private float waitTime;

	void Start() {
		waitTime = spawnRate;
        itemData = new ItemData();
        itemData.position = new Position();
	}
	
	// Update is called once per frame
	void Update () {
        if (waitTime <= 0) {
            itemData.position.x = spawnPos [whatToSpawn].position.x.TwoDecimals();
            itemData.position.y = spawnPos[whatToSpawn].position.y.TwoDecimals();
            whatToSpawn = Random.Range (0, spawnPos.Length);
            Instantiate(item, spawnPos[whatToSpawn].position, Quaternion.identity);
            waitTime = spawnRate;

            networkIdentity.GetSocket().Emit("spawnHeal", new JSONObject(JsonUtility.ToJson(itemData)));
			
		} else {
			waitTime -= Time.deltaTime;
		}
	}
}