var io = require('socket.io')(process.env.PORT || 52335)

var Player = require('./Player.js');
var Heal = require('./Heal.js');

console.log('Server Start');

var players = [];
var sockets = [];
var heals = [];

setInterval(() => {
    heals.forEach(heal => {
        var isDestroyed = heal.onUpdate();

        if(isDestroyed){
            var index = heals.indexOf(heal);
            if(index > -1){
                heals.splice(index, 1);

                var returnData = {
                    id: heal.id
                }

                for(var playerID in players){
                    sockets[playerID].emit('serverUnspawn', returnData);
                }
            } else {
                var returnData = {
                    id: heal.id,
                    position: {
                        x: heal.position.x,
                        y: heal.position.y
                    }
                }

                for(var playerID in players){
                    sockets[playerID].emit('updatePosition', returnData);
                }
            }
        }
    });
}, 100, 0);

io.on('connection', function(socket){
    console.log('Connected!');

    var player = new Player();
    var thisPlayerID = player.id;

    players[thisPlayerID] = player;
    sockets[thisPlayerID] = socket;

    socket.emit('register', {id: thisPlayerID});
    socket.emit('spawn', player);
    socket.broadcast.emit('spawn', player)
    
    for(var playerID in players){
        if(playerID != thisPlayerID){
            socket.emit('spawn', players[playerID]);
        }
    }

    socket.on('updatePosition', function(data){
        player.position.x = data.position.x;
        player.position.y = data.position.y;

        socket.broadcast.emit('updatePosition', player)
    });

    socket.on('updateRotation', function(data){
        player.playerRotation = data.playerRotation;

        socket.broadcast.emit('updateRotation', player);
    });

    socket.on('spawnHeal', function(data){
        var heal = new Heal();
        heal.name = 'Heal';
        heal.position.x = data.position.x;
        heal.position.y = data.position.y;

        heals.push(heal);

        var returnData ={
            name: heal.name,
            id: heal.id,
            position: {
                x: heal.position.x,
                y: heal.position.y
            }
        }
        socket.emit('serverSpawn', returnData);
        socket.broadcast.emit('serverSpawn', returnData);
    });

    socket.on('disconnect', function(){
        console.log('Disconnected!');
        delete players[thisPlayerID];
        delete sockets[thisPlayerID];
        socket.broadcast.emit('disconnected', player);
    });

    socket.on('playerDie', function(){
        console.log('Playerdied!');
        delete players[thisPlayerID];
        delete sockets[thisPlayerID];
        socket.broadcast.emit('playerDie', player);
    });
});

function Interval(func, wait, times){
    var intv = function(w,t){
        return function(){
            if(typeof t === "undefined" || t-- > 0){
                setTimeout(intv, w);
                try{
                    func.call(null);
                } catch(e){
                    t=0;
                    throw e.toString();
                }
            }
        };
    }
    (wait,times);
    setTimeout(intv, wait);
}