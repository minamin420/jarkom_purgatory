var shortID = require('shortid');
var Vector2 = require('./Vector2.js')
module.exports = class Player{
    constructor(){
        this.username ="";
        this.id = shortID.generate();
        this.position = new Vector2();
        this.playerRotation = new Number(0);
        this.health = new Number(100);
        this.dead = false;
    }

    damageOut(amount = Number){
        this.health = this.health - amount;

        if (this.health <= 0){
            this.dead = true;
        }

        return this.dead;
    }
}